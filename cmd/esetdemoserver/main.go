package main

import (
	"context"
	"errors"
	"flag"
	"html/template"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/michalsingr/esetdemoserver/config"
	"gitlab.com/michalsingr/esetdemoserver/internal/http/handler"
	httpserver "gitlab.com/michalsingr/esetdemoserver/internal/http/server"
	"gitlab.com/michalsingr/esetdemoserver/internal/service/render"
	"gitlab.com/michalsingr/esetdemoserver/internal/service/sendform"
	"go.uber.org/zap"
)

func main() {
	// load config; without required config values the app
	// will not be able to perform tasks and will not start
	appConfig, err := loadConfig()
	if err != nil {
		panic(err)
	}
	parsedTemplate, err := template.ParseFiles(appConfig.Template.AbsolutePath)
	if err != nil {
		// no template to render -> nothing to do here
		panic("unable to open template file. Please check if provided path is correct and the template content is valid")
	}

	// create cancelable app context, so we can gracefully stop all what's needed
	// e.g. when pod needs to be redeployed or app was killed manually.
	appCtx, cancel := context.WithCancel(context.Background())
	go func() {
		exitChannel := make(chan os.Signal, 1)
		signal.Notify(exitChannel, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
		<-exitChannel
		cancel()
	}()

	// create DI container
	// #authornotes for this test, it's obviously total overkill. But... my experience is
	// that even small apps can be designed as expandable (there is almost no extra effort to make them like that
	// from the very beginning, and you will thank yourself in the future). Usually I use dependency injection
	// principle (and absolutely avoid designs like singleton) supported with auto-wiring like Google Wire or
	// Uber Dig so all dependencies are clear. Adding a new dependency into the system is usually exactly one new line.
	// Dependency principle also helps to easily substitute components with mocks in the tests
	// (which you can't do in singleton world).
	logger := zap.Must(zap.NewProduction())
	renderService := render.NewRenderService(parsedTemplate)
	server := httpserver.NewServer(
		appConfig.Server,
		logger,
		handler.NewSendFormHandler(logger, sendform.NewSendFormService()),
		handler.NewRenderHandler(logger, renderService),
	)

	// template was successfully loaded, but we can validate if it works on some dummy data...
	// otherwise it will emit an error on every request which is completely useless
	_, err = renderService.Render(context.Background(), render.ThreatData{})
	if err != nil {
		panic("template provided is invalid. Please check if you named the fields properly and have correct template syntax")
	}

	// start app with graceful stop
	Run(appCtx, server)
}

// loadConfig tries to get configuration from flags.
// If required fields are not set, an error is returned as the app can't work properly.
func loadConfig() (*config.Config, error) {
	// #authornotes Normally in RL this should be taken from e.g. config yaml / toml that can
	// be overridden by e.g. ENV locally or from k8s environment. Also, I would use e.g. Viper
	// package that provides more convenient work with flags / configs than built-in 'flag' package.
	port := flag.String("p", "", "port to start the server on localhost")
	templatePath := flag.String("t", "", "absolute path to the template file")
	flag.Parse()
	if port == nil || *port == "" {
		return nil, errors.New("please provide port to start the server, e.g. '-p 12345'")
	}
	if templatePath == nil || *templatePath == "" {
		return nil, errors.New("please provide absolute path to the template, e.g. '-t /tmp/mytemplate.tmpl'")
	}

	return &config.Config{
		Server: &config.Server{
			Addr: ":" + *port,
		},
		Template: &config.Template{
			AbsolutePath: *templatePath,
		},
	}, nil
}
