package main

import (
	"context"
	"errors"
	"fmt"

	"golang.org/x/sync/errgroup"
)

// GracefulComponent is a simple interface that must be fulfilled
// to be able to stop our components gracefully.
type GracefulComponent interface {
	Start() error
	GracefulStop() error
}

// Run starts each provided component and performs
// graceful stop when application is going to terminate.
func Run(
	ctx context.Context,
	gracefulComponent ...GracefulComponent,
) {
	// #authornotes for this test it's just overkill... In real app, there are multiple ways to solve this.
	// I usually use 'errgroup' to handle multiple components that has to be stopped
	// gracefully (server, Prometheus server, some connections somewhere etc.).
	group, groupCtx := errgroup.WithContext(ctx)
	for _, component := range gracefulComponent {
		safeComponent := component // this is not necessary in go 1.22 (loop variable component captured by func literal)
		group.Go(func() error {
			return safeComponent.Start()
		})
		group.Go(func() error {
			<-groupCtx.Done()
			return safeComponent.GracefulStop()
		})
	}

	if err := group.Wait(); err != nil {
		fmt.Println(errors.Join(errors.New("an error occurred during application run"), err))
	}
}
