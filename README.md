# ESET Threat Data Renderer

ESET threat data renderer is a simple HTML server that shows input form and after submitting it renders the provided data.

Start the server by providing port and absolute path to the template (you can use [the one included](defaulttemplate.tmpl)):

```shell
go run ./cmd/esetdemoserver -p 12345 -t /tmp/template.tmpl
```

Then open `http://localhost:12345` (or the one with the port you specified) in your browser. Paste JSON data into the input textarea, e.g.:
```json
{
  "threatName": "Win32/Rbot",
  "category": "trojan",
  "size": 437289,
  "detectionDate": "2019-04-01",
  "variants": [
    {
      "name": "Win32/TrojanProxy.Emotet.A",
      "dateAdded": "2019-04-10"
    }
  ]
}
```

After submitting the form, it should be rendered in your HTML template. You can also use HTTP client to call the API - see [Open API](openapi.yaml) specification for further details.
