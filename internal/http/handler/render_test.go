package handler

// #authornotes Normally I would cover handler by the tests using httptest.Server
// and e.g. testify suite. The test should ideally cover 100 % of all states (all kinds of responses
// the handler can return - 200, 4xx, 5xx etc.)
