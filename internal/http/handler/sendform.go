package handler

import (
	"net/http"

	"gitlab.com/michalsingr/esetdemoserver/internal/service/sendform"
	"go.uber.org/zap"
)

// SendFormHandler serves HTML form for sending thread JSON data to the render endpoint.
type SendFormHandler struct {
	logger      *zap.Logger
	sendService *sendform.Service
}

func NewSendFormHandler(
	logger *zap.Logger,
	sendFormService *sendform.Service,
) *SendFormHandler {
	return &SendFormHandler{
		logger:      logger,
		sendService: sendFormService,
	}
}

func (h *SendFormHandler) ShowSendForm(responseWriter http.ResponseWriter, request *http.Request) {
	// wrong method or path
	if request.Method != http.MethodGet {
		responseWriter.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	if request.URL.Path != "/" {
		responseWriter.WriteHeader(http.StatusNotFound)
		return
	}

	output, err := h.sendService.Render(request.Context())
	if err != nil {
		// #authornotes for the sake of this test, we always return 5xx.
		// But in RL, we should return status based on business logic,
		// e.g. not found, unauthorized etc.
		responseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}

	// #authornotes normally, this should be handled by some middleware
	responseWriter.Header().Set("Content-Type", "text/html; charset=UTF-8")
	_, err = responseWriter.Write([]byte(output))
	if err != nil {
		h.logger.With(zap.Error(err)).Error("unable to write response")
	}
}
