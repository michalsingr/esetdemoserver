package handler

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"
	texttemplate "text/template"

	"gitlab.com/michalsingr/esetdemoserver/internal/service/render"
	"go.uber.org/zap"
)

// RenderHandler for processing requests (JSON in 'json_input' form field) from SendFormHandler.
type RenderHandler struct {
	logger        *zap.Logger
	renderService *render.Service
}

func NewRenderHandler(
	logger *zap.Logger,
	renderService *render.Service,
) *RenderHandler {
	return &RenderHandler{
		logger:        logger,
		renderService: renderService,
	}
}

func (h *RenderHandler) Render(responseWriter http.ResponseWriter, request *http.Request) {
	if request.Method != http.MethodPost {
		responseWriter.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	err := request.ParseForm()
	if err != nil {
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}

	var threatData render.ThreatData
	err = json.NewDecoder(strings.NewReader(request.Form.Get("json_input"))).Decode(&threatData)
	if err != nil {
		// no request body, wrong form data, wrong data structure, wrong types etc. -> bye
		// #authornotes In RL, this should be processed by some validator and some meaningful response should be
		// returned so the client knows what should be corrected.
		responseWriter.WriteHeader(http.StatusBadRequest)
		return
	}

	output, err := h.renderService.Render(request.Context(), threatData)
	if err != nil {
		// user's fault, e.g. wrong placeholders used in the template -> bad request
		// the other approach could be validating some data on the start upon the provided template
		// #authornotes this should never happen as we validate the template at the start.
		// But as the service is designed as reusable, we check it here too and will check on all other potential
		// places (e.g. Kafka producer) to handle the errors properly.
		var execError texttemplate.ExecError
		if errors.As(err, &execError) {
			responseWriter.WriteHeader(http.StatusBadRequest)
			return
		}
		responseWriter.WriteHeader(http.StatusInternalServerError)
		return
	}

	// #authornotes normally, this should be handled by some middleware
	responseWriter.Header().Set("Content-Type", "text/html; charset=UTF-8")
	_, err = responseWriter.Write([]byte(output))
	if err != nil {
		h.logger.With(zap.Error(err)).Error("unable to write response")
	}
}
