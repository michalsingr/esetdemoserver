package server

import (
	"errors"
	"net/http"
	"time"

	"gitlab.com/michalsingr/esetdemoserver/config"
	"gitlab.com/michalsingr/esetdemoserver/internal/http/handler"
	"go.uber.org/zap"
)

// Server is a simple wrapper over standard HTTP server.
type Server struct {
	server *http.Server
	logger *zap.Logger
}

func NewServer(
	config *config.Server,
	logger *zap.Logger,
	sendHandler *handler.SendFormHandler,
	renderHandler *handler.RenderHandler,
) *Server {
	// #authornotes pure golang handler is a pain. I would normally use
	// some designated lib like chi or gorilla mux.
	newHandler := http.NewServeMux()
	newHandler.HandleFunc("/", sendHandler.ShowSendForm)
	newHandler.HandleFunc("/render", renderHandler.Render)

	return &Server{
		server: &http.Server{
			Addr:              config.Addr,
			Handler:           newHandler,
			ReadHeaderTimeout: time.Second, // should be from the config
		},
		logger: logger,
	}
}

func (s *Server) Start() error {
	s.logger.Info("app HTTP server: starting on " + s.server.Addr)
	err := s.server.ListenAndServe()
	if err != nil && !errors.Is(err, http.ErrServerClosed) {
		return err
	}

	return nil
}

func (s *Server) GracefulStop() error {
	s.logger.Info("app HTTP server: stopping gracefully...")
	return s.server.Close()
}
