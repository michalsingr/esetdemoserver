package render

import (
	"context"
	"errors"
	"html/template"
	"testing"
	texttemplate "text/template"

	"github.com/stretchr/testify/suite"
)

func TestRenderTestSuite(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(renderTestSuite))
}

type renderTestSuite struct {
	suite.Suite
}

func (s *renderTestSuite) TestHappyScenario() {
	parsedTemplate, err := template.New("test").Parse(`{{.ThreatName}} {{.Category}} {{.Size}}`) // just for demo, not all fields tested
	s.Require().NoError(err)
	service := NewRenderService(parsedTemplate)
	result, err := service.Render(context.Background(), ThreatData{
		ThreatName: "threat",
		Category:   "category",
		Size:       1,
	})
	s.Assert().NoError(err)
	s.Assert().Equal("threat category 1", result)
}

func (s *renderTestSuite) TestErrorEmptyTemplate() {
	service := NewRenderService(template.New("test")) // empty template provided
	result, err := service.Render(context.Background(), ThreatData{
		ThreatName: "threat",
		Category:   "category",
		Size:       1,
	})
	s.Assert().Equal("", result)
	s.Assert().ErrorContains(err, `template: "test" is an incomplete or empty template`)
}

func (s *renderTestSuite) TestErrorInvalidTemplate() {
	parsedTemplate, err := template.New("test").Parse(`{{.UnknownField}}`) // this will end with an error
	s.Require().NoError(err)
	service := NewRenderService(parsedTemplate) // empty template provided
	result, err := service.Render(context.Background(), ThreatData{
		ThreatName: "threat",
		Category:   "category",
		Size:       1,
	})
	s.Assert().Equal("", result)
	var execError texttemplate.ExecError
	s.Assert().True(errors.As(err, &execError))
	s.Assert().ErrorContains(err, `can't evaluate field UnknownField in type`)
}
