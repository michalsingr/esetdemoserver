package render

import (
	"bytes"
	"context"
	"html/template"
)

type Service struct {
	template *template.Template
}

func NewRenderService(template *template.Template) *Service {
	return &Service{
		template: template,
	}
}

func (s *Service) Render(_ context.Context, input ThreatData) (string, error) {
	newBuffer := bytes.NewBuffer(nil)
	err := s.template.Execute(newBuffer, input)
	if err != nil {
		return "", err
	}

	return newBuffer.String(), nil
}
