package render

import (
	"strconv"
	"time"
)

type ThreatData struct {
	ThreatName    string    `json:"threatName"`
	Category      string    `json:"category"`
	Size          int64     `json:"size"`
	DetectionDate LocalDate `json:"detectionDate"` // #authornotes rendering zero value as requested does not make any sense...
	Variants      []Variant `json:"variants"`
}

type Variant struct {
	Name      string    `json:"name"`
	DateAdded LocalDate `json:"dateAdded"` // #authornotes rendering zero value as requested does not make any sense...
}

// LocalDate - as datetime in the input is not in standardized RFC format,
// this crap for processing custom format needs to be used.
type LocalDate struct {
	time.Time
}

func (ld *LocalDate) UnmarshalJSON(bytes []byte) error {
	unquoted, err := strconv.Unquote(string(bytes))
	if err != nil {
		return err
	}
	parsedTime, err := time.Parse(time.DateOnly, unquoted)
	if err != nil {
		return err
	}
	ld.Time = parsedTime

	return nil
}

func (ld LocalDate) MarshalJSON() ([]byte, error) {
	newString := ld.Format(strconv.Quote(time.DateOnly))

	return []byte(newString), nil
}
