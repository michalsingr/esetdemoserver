package sendform

import "context"

const template = `<!DOCTYPE html>
<html>
<body>
<form action="/render" method="POST">
<label for="json_input">JSON input:</label><br/>
<textarea rows="20" cols="70" id="json_input"  name="json_input">
</textarea><br/>
<input type="submit" value="Submit"/>
</form>
</body>
</html>`

type Service struct{}

func NewSendFormService() *Service {
	return &Service{}
}

func (s *Service) Render(_ context.Context) (string, error) {
	// #authornotes this is weird... for the sake of this test, it looks like this. IMHO standard approach would be
	// not having this part at all and accept directly POST /render requests in some standard (e.g. JSON) format.
	return template, nil
}
