package config

type Config struct {
	Server   *Server
	Template *Template
}

type Server struct {
	Addr string
}

type Template struct {
	AbsolutePath string
}
